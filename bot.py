import discord
from discord.ext import commands
import asyncio
import aiohttp
import json

import config

description = """Tökmindegy Bot

Tanácsadóink szerint célunk: aggregálni a felvállalható feladatmegosztást."""

bot = commands.Bot(command_prefix="$", description=description)

@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('-' * 6)

@bot.command()
async def ping():
    """Sends a Ping! response"""
    await bot.say("Pong!")

@bot.group(pass_context=True)
async def gif(ctx):
    if ctx.invoked_subcommand is None:
        await bot.say('`Available subcommands are: random`')
        return

@gif.command()
async def random():
    async with aiohttp.get('http://api.giphy.com/v1/gifs/random?api_key=' + config.GIPHY_TOKEN) as req:
        if req.status == 200:
            json_data = json.loads(await req.text())
            await bot.say(json_data["data"]["images"]["source"]["url"])
        else:
            await bot.say("**Hiba!** Nincs találat!")


bot.run(config.TOKEN, reconnect=True)